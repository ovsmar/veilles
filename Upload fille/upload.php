<?php
$target_dir = "uploads/"; //!spécifie le répertoire où le fichier va être placé
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]); //!spécifie le chemin du fichier à uploader
$uploadOk = 1; //! 1-uploader ; 0- non
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION)); //!contient l'extension de fichier du fichier

//!Générer un nom aléatoire!
if(isset($_POST["submit"])) {
  $random = rand(0,9999); //! "rand" Génère une valeur aléatoire
  $name_new = sha1($target_file.$random); //!pour calculer le hachage SHA-1 d'une chaîne
  echo $name_new;
}
//!vérifier si le fichier image est une image réelle ou une fausse image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]); //!""getimagesize Retourne la taille d'une image
  if($check !== false) {
    echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    echo "File is not an image.";
    $uploadOk = 0;
  }
}

//!Limiter les double extension
if (preg_match('/\.(php\d?|phtml|phar|pHp|perl|php3|php4|pl|py|jsp|asp|htm|shtml|sh|cgi|js)(\.|\z)/', $target_file)) { //! "preg_match"Effectue une recherche de correspondance
  echo "File is not an image.";
  $uploadOk = 0;
}
   

//!Vérifier si le fichier existe déjà
if (file_exists($target_file)) {   //!"file_exists" Vérifie si un fichier ou un dossier existe
  echo "Sorry, file already exists.";
  $uploadOk = 0;
}

//!Limiter la taille du fichier
if ($_FILES["fileToUpload"]["size"] > 300000) {
  echo "Sorry, your file is too large.";
  $uploadOk = 0;
}

//!Limiter  les types de format d'images 
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") { //!  "!=" si $a est différent de $b 
  echo "Sorry, only JPG, JPEG & PNG  files are allowed.";
  $uploadOk = 0;
}

//!Vérifiez si $uploadOk est défini sur 0 par une erreur
if ($uploadOk == 0) {
  echo "Sorry, your file was not uploaded.";
//!si tout va bien, essayez de uploader le fichier
} else {
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
  } else {
    echo "Sorry, there was an error uploading your file.";
  }



//!lire les octes de l'image'
if(isset($_POST["submit"])) {
  
//!définir le chemin du dossier
$filename = $target_file;

//!Open a binary file with read mode
$target_dir = fopen ( $filename , "rb" ) ;

//!Lire le contenu du fichier
$content = fread ( $target_dir ,filesize($filename)) ;

//!Encoder le contenu à l'aide de la méthode base64_encode()
$encoded_data = base64_encode($content);

//!Définir le type mime
$mime_type ='image/*';

//!Définissez la chaîne binaire pour générer l'image
$binary_data = 'data:' . $mime_type . ';base64,' . $encoded_data ;

//!Afficher 
echo $content;

//!Afficher l'image
echo '<img src="'.$binary_data.'" height="200px" width="250px">';

//!Fermer le fichier
fclose ( $target_dir ) ;
 }

  
}

?>