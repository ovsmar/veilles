const fs = require("fs");

let fichier = fs.readFileSync("data/personal.json");
let personnes = JSON.parse(fichier);

//*Functional Map
// TODO:   Un exemple qui convertit tous les noms en majuscules :
//!La méthode toUpperCase() retourne la valeur de la chaîne courante, convertie en majuscules.

let names = personnes.map((personnes) => personnes.name.toUpperCase());

console.log(names);

//*

//*Functional Filter
// TODO: Un exemple qui lister le nombre de personnes de moins de 45 ans habitant en France :

let getPerson = personnes.filter(
  (personne) => personne.age < 45 && personne.country === "France"
);

console.log(getPerson);

//*

//*Functional Reduce
// TODO: Un exemple qui calculer le nombre total d’enfant de la liste des personnes :

let nbChildTotal = personnes.reduce(
  (accumulator, curr) => accumulator + curr.nb_child,
  0
);

console.log(nbChildTotal);

//*

//*Functional map , filter , reduce
let Allperson = personnes.map((personnes) => personnes);
let totalage = Allperson.filter((per) => per.id > 120).reduce(
  (total, amount) => total + amount.nb_child,
  0
);

console.log(totalage);
//*
